#!/usr/bin/env python3
from PyQt5.QtGui import QGuiApplication
from PyQt5.QtQml import QQmlApplicationEngine
from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot, QUrl, QTimer, QAbstractTableModel, Qt
from PyQt5.QtQuick import QQuickView
import lib2048
import sys
import copy
import pprint

class GameModel(QAbstractTableModel):
    def __init__(self, parent=None, size=None):
        QAbstractTableModel.__init__(self, parent)
        if not size:
            size = 4
        self._game = lib2048.lib2048()

    def rowCount(self, parent):
        return len(self._game._board)

    def columnCount(self, parent):
        return len(self._game._board[0])

    def data(self, index, role):
        if not index.isValid():
            return None
        if role != Qt.DisplayRole:
            return None
        return self._game._board[index.row()][index.column()]

    @pyqtSlot(int)
    def shift(self, direction):
        if direction in (0,1,2,3):
            self.layoutAboutToBeChanged.emit()
            [ self._game.shift_left, self._game.shift_right,
             self._game.shift_up, self._game.shift_down ][direction]()
            #print("shifted", ['left','right','up','down'][direction])
            self.layoutChanged.emit()


if __name__ == "__main__":
    app = QGuiApplication(sys.argv)
    engine = QQmlApplicationEngine()
    game = GameModel()
    engine.rootContext().setContextProperty("game", game)
    engine.load(QUrl("py2048-qml.qml"))

    #timer är till för att kunna stänga programmet med Ctrl-C
    timer = QTimer()
    timer.timeout.connect(lambda: None)
    timer.start(100)

    engine.quit.connect(app.quit)
    if not engine.rootObjects():
        print('no root objects')
        sys.exit(app.exec_())
    sys.exit(app.exec())
