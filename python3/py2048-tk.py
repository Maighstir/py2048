#!/usr/bin/env python3
import lib2048
from tkinter import Canvas, Tk

class NumBox(Canvas):
    _colours=((238, 238, 238), # 0
             (124, 181, 226), ( 68, 149, 212), ( 47, 104, 149), # 2, 4, 8
             (245, 189, 112), (242, 160,  50), (205, 136,  41), # 16, 32, 64
             (227, 112,  81), (222,  88,  51), (189,  74,  43), # 128, 256, 512
             ( 84,  84, 218), ( 59,  60, 153), (255, 215,   0)) # 1024, 2048, 4096+
    _size = 64
    def __init__(self, parent, val=None):
        super().__init__(parent, height=self._size, width=self._size)
        self.setval(val or 0)
        return

    def setval(self,v):
        self.value = v
        if self.value >= 4096:
            self.colour = self._colours[-1]
        else:
            for c in range(len(self._colours)):
                if 2**c >= self.value:
                    self.colour = self._colours[c]
                    break
        self.delete("all")
        self.config(background="#%02x%02x%02x" % self.colour)
        _bg_luminance = int(round((.299*self.colour[0])+(.587*self.colour[1])+(.114*self.colour[2])))
        self.text_colour = 'black' if _bg_luminance >=128 else 'white'
        if self.value > 0:
            self.create_text((round(self._size/2),round(self._size/2)), text=str(v), font=("Helvetica", 14, "bold"), fill=self.text_colour)

class GameApp:
    def __init__(self, parent):
        self.game = lib2048.lib2048()
        self.wintitle = "py2048"
        self.parent = parent
        self.parent.title(self.wintitle)
        self.display = []
        for r in range(len(self.game.board)):
            self.display.append([])
            for c in range(len(self.game.board[r])):
                self.display[r].append(NumBox(parent,self.game.board[r][c]))
                self.display[r][c].grid(row=r, column=c)

    def paintboard(self, board):
        for r in range(len(board)):
            for c in range(len(board[r])):
                self.display[r][c].setval(board[r][c])
                if self.game.score > 0:
                    self.parent.title(self.wintitle+": "+str(self.game.score)+['',' !!',' ...'][self.game.status])

    def left(self, event):
        self.game.shift_left()
        self.paintboard(self.game.board)

    def right(self, event):
        self.game.shift_right()
        self.paintboard(self.game.board)

    def up(self, event):
        self.game.shift_up()
        self.paintboard(self.game.board)

    def down(self, event):
        self.game.shift_down()
        self.paintboard(self.game.board)

root = Tk()

game = GameApp(root)

root.bind('<Left>', game.left)
root.bind('a', game.left)
root.bind('h', game.left)
root.bind('<Right>', game.right)
root.bind('d', game.right)
root.bind('l', game.right)
root.bind('<Up>', game.up)
root.bind('w', game.up)
root.bind('k', game.up)
root.bind('<Down>', game.down)
root.bind('s', game.down)
root.bind('j', game.down)
root.resizable(0,0)

root.mainloop()
