# py2048
A Python implementation of the game 2048.

Python 2 versionruns (at least) under Python 2.3.3 (last version for Mac OS 9).
Python 2 Curses interface also runs under Python 3.9.9, so that's neat.

Python 3 version most likely does NOT run under Python 2.

## lib2048.py
The base game library is used by the various user interfaces for game logic.

## TTY interface (py2048-tty.py)
Doesn't assume anything from the capabilities of your terminal, and prints the
score and full board after every choice. Not really useful after video
terminals appeared in the 60's.

## Curses interface (py2048-curses.py)
The curses interface shows the game in a text terminal.

Use arrow keys, wasd, or hjkl to play, and "Q" to quit.

## Tk interface (py2048-tk.py)
The graphical Tk interface is slightly more fancy than curses as it has
colours for the different numbers. It also requires a graphical environment.

Use arrow keys, wasd, or hjkl to play. Quit by closing the window.
Score is displayed in the title bar.

## QML interface (py2048-qml.py, py2048-qml.qml)
Trying to write a similar UI to the Tk one, in QML and PyQt5.
Flickers for some reason, and sometimes doesn't update the board display
(despite the in-memory board having changed).

Use arrow keys, wasd, or hjkl to play. Quit by closing the window.
No score yet.
