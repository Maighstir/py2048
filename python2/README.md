# py2048/python2
A Python2 implementation of the game 2048.
Primarily created in order to port the original Python3 code to Python2.3.3
so that it can run on Mac OS 9. It's otherwise very similar to its Python3
parent.

## lib2048.py
The base game library is used by the various user interfaces for game logic.

## Curses interface (py2048-curses.py)
The curses interface shows the game in a text terminal.

Use arrow keys, wasd, or hjkl to play, and "Q" to quit.

## Tk interface (py2048-tk.py)
The graphical Tk interface is slightly more fancy than curses as it has
colours for the different numbers. It also requires a graphical environment.

Use arrow keys, wasd, or hjkl to play. Quit by closing the window.
Score is displayed in the title bar.
